CXX = g++
CPPFLAGS = -std=gnu++11 -Wall -Wextra -I include
LIBS = -lboost_system -lboost_filesystem -lboost_regex
DEPS = \
	src/main.cpp            \
	src/shell.cpp           \
	src/fsop.cpp            \
	src/add_rm_edit.cpp     \
	src/list.cpp            \
	src/srch_shw_ren.cpp    \
	src/permexception.cpp   \
	src/permmanager.cpp

filesystem: $(DEPS)
	$(CXX) -o $@ $^ $(CPPFLAGS) $(LIBS) -g

clean: 
	rm -f filesystem
