# Operating System Final Project
Create a file system with permission design (haven't been done) and some basic commands for file operations

## Requirement
* gnome-terminal
* libboost-all-dev

## Commands
* cc (clean screen)
* cf (create file)
* rm (remvoe file)
* ef (edit file)
* cp (copy file)
* mv (move file)
* ls (list file)
* sm (show mode)
* tree (show whole files in the filesystem)
* find (search file)
* cat (show file content)
* ren (rename file)
* chm (chmod file)

## Setup
* Enter script and run setup.sh
* Change directory to "/tmp/fs/bin/"
* Execute "filesystem" with `./filesystem`

## Screenshot
![](https://i.imgur.com/OZR8PBm.png)
