#ifndef PERMEXCEPTION_H
#define PERMEXCEPTION_H
#include <string>
#include <exception>

#define THROW_PERMISSION_EXCEPTION(MSG) throw permexception(__FILE__, __func__, __LINE__, MSG)
#define THROW_PERMISSION_EXCEPTION_TRACE() throw permexception(__FILE__, __func__, __LINE__, "---STACKTRACE---")

class permexception : public std::exception
{
public:
    permexception();
    permexception(std::string msg);
    permexception(const char* file, const char* func, const int line, std::string msg);
    virtual const char* what() const throw();

protected:
    std::string file;
    std::string func;
    std::string line;
    std::string msg;
    std::string err_msg;

};

#endif
