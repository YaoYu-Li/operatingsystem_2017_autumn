#ifndef _ADD_RM_EDIT_H
#define _ADD_RM_EDIT_H

#include <iostream>

using namespace std;

int create_file(string file_name, string file_path, string file_owner, string file_type);
int delete_file(string file_path);
void edit_file(const char *file_path);
int copy_file(string file_path, string file_destination);
int move_file(string file_path, string file_destination);
#endif
