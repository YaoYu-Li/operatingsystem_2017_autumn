#ifndef PERMMANAGER_H
#define PERMMANAGER_H

#include <string>
#include <vector>

void chmod(std::string filename, std::string permission);

namespace perm
{
    class PermManager
    {
    public:
        /**
         * Get the attributes of a file
         *
         * Parameters:
         * filename         -- file name
         * readable         -- a boolean value that will save the readable attribute (For owner)
         * writable         -- a boolean value that will save the writable attribute (For owner)
         * executable       -- a boolean value that will save the executable attribute (For owner)
         * group_readable   -- a boolean value that will save the readable attribute (For group)
         * group_writable   -- a boolean value that will save the writable attribute (For group)
         * group_executable -- a boolean value that will save the executable attribute (For group)
         * other_readable   -- a boolean value that will save the readable attribute (For others)
         * other_writable   -- a boolean value that will save the writable attribute (For others)
         * other_executable -- a boolean value that will save the executable attribute (For others)
         *
         * Exceptions:
         * permexception - table.txt not found, files not found, arguments contain null pointer and bitfield error
         */
        void getPermission(const std::string filename, bool* readable, bool* writable, bool* executable,
                           bool* group_readable, bool* group_writable, bool* group_executable, bool* other_readable,
                           bool* other_writable, bool* other_executable);

        /**
         * Set the attributes of a file
         *
         * Parameters:
         * filename         -- file name
         * readable         -- set readable attribute to the file (For owner)
         * writable         -- set writable attribute to the file (For owner)
         * executable       -- set exetuable attribute to the file (For owner)
         * group_readable   -- set readable attribute to the file (For group)
         * group_writable   -- set writable attribute to the file (For group)
         * group_executable -- set exetuable attribute to the file (For group)
         * other_readable   -- set readable attribute to the file (For others)
         * other_writable   -- set writable attribute to the file (For others)
         * other_executable -- set exetuable attribute to the file (For others)
         *
         * Exceptions:
         * permexception - table.txt not found, files not found and bitfield error
         */
        void setPermission(const std::string filename, bool readable = true, bool writable = true,
                           bool executable = true, bool group_readable = true, bool group_writable = true,
                           bool group_executable = true, bool other_readable = true, bool other_writable = true,
                           bool other_executable = true);

        /**
         * Request whether a file is readable
         *
         * Parameters:
         * filename  -- file name
         * requester -- requester
         *
         * Return:
         * True  -- readable
         * False -- unreadable
         *
         * Exceptions:
         * permexception
         */
        bool isReadable(const std::string filename, const std::string requester);


        /**
         * Request whether a file is writable
         *
         * Parameters:
         * filename  -- file name
         * requester -- requester
         *
         * Return:
         * True  -- writable
         * False -- unwritable
         * 
         * Exceptions:
         * permexception
         */
        bool isWritable(const std::string filename, const std::string requester);

    };
}


#endif
