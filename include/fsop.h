#ifndef _FSOP_H
#define _FSOP_H

#include <string>
#include <vector>

using namespace std;

class FsOperation {
public:
    FsOperation();

    void exec_create_file(vector<string> vec_cmd);

    void exec_delete_file(vector<string> vec_cmd);

    void exec_edit_file(vector<string> vec_cmd);

    void exec_copy_file(vector<string> vec_cmd);

    void exec_move_file(vector<string> vec_cmd);

    void exec_list_files(vector<string> vec_cmd);

    void exec_show_tree(vector<string> vec_cmd);

    void exec_search_file(vector<string> vec_cmd);

    void exec_show_content(vector<string> vec_cmd);

    void exec_rename_file(vector<string> vec_cmd);

    void exec_chmod(vector<string> vec_cmd);

    void exec_show_perm(vector<string> vec_cmd);

    void exec_show_unknown_cmd(vector<string> vec_cmd);
};

#endif
