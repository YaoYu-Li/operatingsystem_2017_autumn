#ifndef _SRCH_SHW_REN_H
#define _SRCH_SHW_REN_H

#include <iostream>
using namespace std;

void search_file(string file_name);
void show_file_content(string path);
void rename_file(string path, string name);

#endif
