#ifndef _SHELL_H
#define _SHELL_H

#include <string>
#include <vector>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define BOLD  "\033[1m"
#define BOLD_END  "\033[0m"

using namespace std;

class Shell {
public:
    Shell();
    Shell(string _prompt);

    void show_usage();

    void show_prompt();

    void clear_cmd();

    void read_input();

    void parse_input();

    void exec_cmd();

private:
    string _prompt;
    string _cmd; /* cmd string */
    vector<string> _vec_cmd; /* string vector parsed by space */
};

#endif
