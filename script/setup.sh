#!/usr/bin/env bash

# change current directory to root of repository
echo change direcotory to root of repository
cd ..

# generate binary file
echo make executable file
make

# remove original /tmp/fs/
echo remove \"/tmp/fs/\"
rm -rf /tmp/fs/

# copy basic emulated file system to /tmp
echo copy \"fs/\" directory to /tmp/
cp -r fs/ /tmp/

# copy binary file to /tmp/
echo copy binary executable file \"filesystem\"  directory to /tmp/bin/
cp filesystem /tmp/fs/bin/
