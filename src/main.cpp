#include <iostream>

#include "shell.h"

int main () 
{
    Shell shell;

    shell.show_usage();
    
    while (true) {
        shell.clear_cmd();
        shell.show_prompt();
        shell.read_input();
        shell.parse_input();
        shell.exec_cmd();
    }
}
