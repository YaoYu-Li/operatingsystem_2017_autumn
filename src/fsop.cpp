#include <iostream>
#include <string>
#include <vector>
#include <string.h>

#include "fsop.h"
#include "add_rm_edit.h"
#include "srch_shw_ren.h"
#include "list.h"
#include "permmanager.h"
#include "permexception.h"

using namespace std;

static char **convert_string_to_chararray(vector<string> str_vec)
{
    char **cstrings = new char *[str_vec.size()];
    
    for (size_t i = 0; i < str_vec.size(); ++i) {
        cstrings[i] = new char [str_vec[i].size()];
        strcpy(cstrings[i], str_vec[i].c_str());
    }

    return cstrings;
}


static void free_chararray(char **chararray, size_t arraysize)
{
    for(size_t i = 0; i < arraysize; ++i) {
        delete chararray[i];
        chararray[i] = NULL;
    }

    delete[] chararray;
    chararray = NULL;
}


static bool is_readable(std::string filename)                                                                                                                                                                                            
{                                                                                                                                                                                                                                         
    try {                                                                                                                                                                                                                                 
        perm::PermManager manager;                                                                                                                                                                                                              
        bool r = false, w = false, x = false;                                                                                                                                                                                             
        bool rg = false, wg = false, xg = false;                                                                                                                                                                                          
        bool ro = false, wo = false, xo = false;                                                                                                                                                                                          
        manager.getPermission(filename, &r, &w, &x, &rg, &wg, &xg, &ro, &wo, &xo);                                                                                                                                                        


        return r;

    } catch(permexception e) {                                                                                                                                                                                                            
        std::cerr << e.what() << std::endl;                                                                                                                                                                                               
        THROW_PERMISSION_EXCEPTION_TRACE();                                                                                                                                                                                               
    }
}


static bool is_writable(std::string filename)                                                                                                                                                                                            
{                                                                                                                                                                                                                                         
    try {                                                                                                                                                                                                                                 
        perm::PermManager manager;                                                                                                                                                                                                              
        bool r = false, w = false, x = false;                                                                                                                                                                                             
        bool rg = false, wg = false, xg = false;                                                                                                                                                                                          
        bool ro = false, wo = false, xo = false;                                                                                                                                                                                          
        manager.getPermission(filename, &r, &w, &x, &rg, &wg, &xg, &ro, &wo, &xo);                                                                                                                                                        

        return w;

    } catch(permexception e) {                                                                                                                                                                                                            
        std::cerr << e.what() << std::endl;                                                                                                                                                                                               
        THROW_PERMISSION_EXCEPTION_TRACE();                                                                                                                                                                                               
    }
}


FsOperation::FsOperation()
{
    
}


void FsOperation::exec_create_file(vector<string> vec_cmd)
{
    int res;

    string argu = vec_cmd.back();
    size_t pos = argu.rfind('/');

    string file_name;
    string file_path;
    string file_owner = "owner";
    string file_type = argu.back() == '/' ? "direction" : "file";
    
    /* 
     * remove the last slash if the file type is direction, and update the new
     * position of the slash
     */
    if (file_type == "direction") {
        argu = argu.substr(0, pos);
        pos = argu.rfind('/');
    }

    if (pos == string::npos) {
        file_name = argu;
        file_path = "/";
    } else {
        file_name = argu.substr(pos + 1);
        file_path = "/" + argu.substr(0, pos);
    }

    if (file_path[1] == '/')
        file_path = file_path.substr(1);

    res = create_file(file_name, file_path, file_owner, file_type);

    if (res == -1) {
        cout << "Directory does not exist!" << endl;
    } else if (res == -2) {
        cout << "File already exists!" << endl;
    }
}


void FsOperation::exec_delete_file(vector<string> vec_cmd)
{
    string argu = vec_cmd.back();

    if (!is_readable(argu) || !is_writable(argu)) {
        cout << "You have no permission to delete " << argu
             << " or " << argu << " does not exist" << endl;
        return;
    }

    delete_file(argu);
}


void FsOperation::exec_edit_file(vector<string> vec_cmd)
{
    char **cstrings = convert_string_to_chararray(vec_cmd);

    if (!is_readable(vec_cmd[1]) || !is_writable(vec_cmd[1])) {
        cout << "You have no permission to read or write " << vec_cmd[1]
             << " or " << vec_cmd[1] << " does not exist" << endl;
        return;
    }

    edit_file(vec_cmd[1].c_str());

    free_chararray(cstrings, vec_cmd.size());
}


void FsOperation::exec_copy_file(vector<string> vec_cmd)
{
    /* 
     * copy the file to another directory without changing file name 
     * e.g., cp /filename /a/filename (filename should be the same)
     */
    copy_file(vec_cmd[1], vec_cmd[2]);
}


void FsOperation::exec_move_file(vector<string> vec_cmd)
{
    /* 
     * only move the file path, but not file name 
     * e.g., mv /filename /a/filename (filename should be the same)
     */

    if (!is_readable(vec_cmd[1]) || !is_writable(vec_cmd[1])) {
        cout << "You have no permission to move " << vec_cmd[1]
             << " or " << vec_cmd[1] << " does not exist" << endl;
        return;
    }

    move_file(vec_cmd[1], vec_cmd[2]);
}


void FsOperation::exec_list_files(vector<string> vec_cmd)
{   
    char **cstrings = convert_string_to_chararray(vec_cmd);

    list(vec_cmd.size(), cstrings);

    free_chararray(cstrings, vec_cmd.size());
}


void FsOperation::exec_show_tree(vector<string> vec_cmd)
{
    char **cstrings = convert_string_to_chararray(vec_cmd);

    tree(vec_cmd.size(), cstrings);

    free_chararray(cstrings, vec_cmd.size());
}


void FsOperation::exec_search_file(vector<string> vec_cmd)
{
    search_file(vec_cmd.back());
}

void FsOperation::exec_show_content(vector<string> vec_cmd)
{
    if (!is_readable(vec_cmd[1])) {
        cout << "You have no permission to read " << vec_cmd[1]
             << " or " << vec_cmd[1] << " does not exist" << endl;
        return;
    }
    
    show_file_content(vec_cmd.back());
}

void FsOperation::exec_rename_file(vector<string> vec_cmd)
{
    /* 
     * arg1 is the /path/to/file/filename, arg2 is the new filename
     */
    if (!is_readable(vec_cmd[1]) || !is_writable(vec_cmd[1])) {
        cout << "You have no permission to rename " << vec_cmd[1]
             << " or " << vec_cmd[1] << " does not exist" << endl;
        return;
    }

    rename_file(vec_cmd[1], vec_cmd[2]);
}

void FsOperation::exec_chmod(vector<string> vec_cmd)
{
    int bitfield = 177;

    if (vec_cmd[2][0] == '1')
        bitfield += 400;

    if (vec_cmd[2][1] == '1')
        bitfield += 200;

    chmod(vec_cmd[1], to_string(bitfield));
}


void FsOperation::exec_show_perm(vector<string> vec_cmd)
{
    string w, r;

    if (is_readable(vec_cmd[1]))
        r = "1";
    else 
        r = "0";

    if (is_writable(vec_cmd[1]))
        w = "1";
    else 
        w = "0";

    cout << "permission: " << r << w << endl;
}


void FsOperation::exec_show_unknown_cmd(vector<string> vec_cmd)
{
    cout << vec_cmd[0] << " is an unknown command!" << endl;
}
