#include <iostream>
#include <fstream>
#include <exception>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include "permmanager.h"
#include "permexception.h"

#ifndef TABLE_PATH
    #define TABLE_PATH "../table.txt"
#endif

using namespace perm;

inline void permission_decode(const int bitfield, bool* readable,
                              bool* writable, bool* executable) __attribute__((always_inline));

void attribute_match(std::string filename, std::string requester, bool* is_owner,
                     bool* is_file_exist);

void PermManager::getPermission(const std::string filename, bool* readable, bool* writable,
                                bool* executable, bool* group_readable, bool* group_writable, bool* group_executable,
                                bool* other_readable, bool* other_writable, bool* other_executable)
{
    if(boost::filesystem::exists(TABLE_PATH) == false) {
        THROW_PERMISSION_EXCEPTION("File " + std::string(TABLE_PATH) + " does not exist");
    }

    if(readable == nullptr) {
        THROW_PERMISSION_EXCEPTION("Argument 'readable' is a null pointer");
    }

    if(writable == nullptr) {
        THROW_PERMISSION_EXCEPTION("[Error] Argument 'writable' is a null pointer");
    }

    bool fileFound = false;
    std::ifstream table {TABLE_PATH};


    try {
        std::string line;
        boost::regex expr{"\\d+, \\d+, \\d+, \\w+, \\d+, ([.\\/\\w]+), (\\d+)"};
        boost::smatch match;
        // For each line
        while(std::getline(table, line)) {
            // If some cases matche pattern
            if (boost::regex_search(line, match, expr)) {
                std::string candidate_filename = match[1];
                std::string candidate_permission = match[2];
                // Check whether the file that we want to find
                if(filename.compare(candidate_filename) == 0) {
                    int perms = std::stoi(candidate_permission);
                    int myperm = perms / 100;
                    int grouperm = (perms - 100 * myperm) / 10;
                    int otherperm = (perms - 100 * myperm - 10 * grouperm);
                    // Decode the bitfield
                    permission_decode(myperm, readable, writable, executable);
                    permission_decode(grouperm, group_readable, group_writable, group_executable);
                    permission_decode(otherperm, other_readable, other_writable, other_executable);
                    fileFound = true;
                    break;
                }
            }
        }
    } catch(permexception e) {
        std::cerr << e.what() << std::endl;
        table.close();
        THROW_PERMISSION_EXCEPTION_TRACE();
    }
    

    table.close();

    /*
    if(fileFound == false) {
        std::cout << "File " << filename << " not found" << std::endl;
    }
    */

}

void PermManager::setPermission(const std::string filename, bool readable, bool writable,
                                bool executable, bool group_readable, bool group_writable, bool group_executable,
                                bool other_readable, bool other_writable, bool other_executable)
{
    if(boost::filesystem::exists(TABLE_PATH) == false) {
        THROW_PERMISSION_EXCEPTION("File " + std::string(TABLE_PATH) + " does not exist");
    }

    bool fileChanged = false;
    bool is_file_found = false;
    std::vector<std::string> filebuf;
    std::ifstream table {TABLE_PATH};

    try {
        std::string line;
        boost::regex expr{"(\\d+, \\d+, \\d+, \\w+, \\d+,) ([.\\/\\w]+), (\\d+)"};
        boost::smatch match;
        // For each line
        while(std::getline(table, line)) {
            // If some cases matche pattern
            if (boost::regex_search(line, match, expr)) {
                std::string other_part = match[1];
                std::string candidate_filename = match[2];
                std::string candidate_permission = match[3];
                // Check whether the file that we want to find
                if(filename.compare(candidate_filename) == 0) {
                    int myperm = 4 * readable + 2 * writable + executable;
                    int grouperm = 4 * group_readable + 2 * group_writable + group_executable;
                    int otherperm = 4 * other_readable + 2 * other_writable + other_executable;
                    if(myperm > 7 || grouperm > 7 || otherperm > 7) {
                        THROW_PERMISSION_EXCEPTION("No such bitfield");
                    }
                    std::string newline = other_part + " " + candidate_filename + ", " + std::to_string(
                                              myperm) + std::to_string(grouperm) + std::to_string(otherperm) + '\n';
                    filebuf.push_back(newline);
                    fileChanged = true;
                    is_file_found = true;
                } else {
                    filebuf.push_back(line + '\n');
                }
            }
        }
    } catch(permexception e) {
        std::cerr << e.what() << std::endl;
        table.close();
        THROW_PERMISSION_EXCEPTION_TRACE();
    }

    table.close();

    if(is_file_found == false) {
        THROW_PERMISSION_EXCEPTION("File " + filename + " not found in table.txt");
    }

    if(fileChanged == true) {
        std::ofstream output{TABLE_PATH};
        try {
            typedef std::vector<std::string>::iterator it_t;
            for(it_t it = filebuf.begin(); it != filebuf.end(); it++) {
                output << *it;
            }
            output.flush();
        } catch(permexception e) {
            std::cerr << e.what() << std::endl;
            output.close();
            THROW_PERMISSION_EXCEPTION_TRACE();
        }
        output.close();
        return;
    }

    THROW_PERMISSION_EXCEPTION("Unknown error");
}

bool PermManager::isReadable(const std::string filename, const std::string requester)
{
    bool is_file_exist = false;
    bool is_owner = false;
    try {
        attribute_match(filename, requester, &is_owner, &is_file_exist);

        if(is_file_exist == true) {
            bool readable = false;
            bool writable = false;
            bool executable = false;
            bool group_readable = false;
            bool group_writable = false;
            bool group_executable = false;
            bool other_readable = false;
            bool other_writable = false;
            bool other_executable = false;
            PermManager::getPermission(filename, &readable, &executable, &writable, &group_readable,
                                       &group_writable, &group_executable,
                                       &other_readable, &other_writable, &other_executable);
            if(is_owner == true) {
                return readable;
            } else {
                return other_readable;
            }
        } else {
            THROW_PERMISSION_EXCEPTION("File " + filename + " does not exist in " + std::string(TABLE_PATH));
        }
    } catch(permexception e) {
        std::cerr << e.what() << std::endl;
        THROW_PERMISSION_EXCEPTION_TRACE();
    }

    THROW_PERMISSION_EXCEPTION("Unknown error");
}

bool PermManager::isWritable(const std::string filename, const std::string requester)
{
    bool is_file_exist = false;
    bool is_owner = false;
    try {
        attribute_match(filename, requester, &is_owner, &is_file_exist);

        if(is_file_exist == true) {
            bool readable = false;
            bool writable = false;
            bool executable = false;
            bool group_readable = false;
            bool group_writable = false;
            bool group_executable = false;
            bool other_readable = false;
            bool other_writable = false;
            bool other_executable = false;
            PermManager::getPermission(filename, &readable, &executable, &writable, &group_readable,
                                       &group_writable, &group_executable,
                                       &other_readable, &other_writable, &other_executable);
            if(is_owner == true) {
                return writable;
            } else {
                return other_writable;
            }
        } else {
            THROW_PERMISSION_EXCEPTION("File " + filename + " does not exist in " + std::string(TABLE_PATH));
        }
    } catch(permexception e) {
        std::cerr << e.what() << std::endl;
        THROW_PERMISSION_EXCEPTION_TRACE();
    }

    THROW_PERMISSION_EXCEPTION("Unknown error");
}

void permission_decode(const int bitfield, bool* readable, bool* writable, bool* executable)
{
    if(bitfield <= 7) {
        *readable = (bitfield & 0x4U) >> 2;
        *writable = (bitfield & 0x2U) >> 1;
        *executable = (bitfield & 0x1U);
    } else {
        THROW_PERMISSION_EXCEPTION("No such bitfield " + std::to_string(bitfield));
    }
}


void chmod(std::string filename, std::string permission)
{
    int perm_all = std::stoi(permission);
    int my_perm = perm_all / 100;
    int group_perm = (perm_all - 100 * my_perm) / 10;
    int other_perm = (perm_all - 100 * my_perm - 10 * group_perm);
    bool r = false, w = false, x = false;
    bool rg = false, wg = false, xg = false;
    bool ro = false, wo = false, xo = false;
    permission_decode(my_perm, &r, &w, &x);
    permission_decode(group_perm, &rg, &wg, &xg);
    permission_decode(other_perm, &ro, &wo, &xo);

    perm::PermManager p;
    p.setPermission(filename, r, w, x, rg, wg, xg, ro, wo, xo);
    
}

void attribute_match(std::string filename, std::string requester, bool* is_owner,
                     bool* is_file_exist)
{

    if(is_owner == nullptr) {
        THROW_PERMISSION_EXCEPTION("argument 'is_owner' is a null pointer");
    }

    if(is_file_exist == nullptr) {
        THROW_PERMISSION_EXCEPTION("argument 'is_file_exist' is a null pointer");
    }

    if(boost::filesystem::exists(TABLE_PATH) == false) {
        THROW_PERMISSION_EXCEPTION("File " + std::string(TABLE_PATH) + " does not exist");
    }

    std::ifstream table {TABLE_PATH};
    try {
        std::string line;
        boost::regex expr{"\\d+, \\d+, \\d+, (\\w+), \\d+, ([.\\/\\w]+), \\d+"};
        boost::smatch match;
        // For each line
        while(std::getline(table, line)) {
            // If some cases matche pattern
            if (boost::regex_search(line, match, expr)) {
                std::string owner = match[1];
                std::string file = match[2];
                if(file.compare(filename) == 0) {
                    *is_file_exist = true;
                    if(owner.compare(requester) == 0) {
                        *is_owner = true;
                    }
                    break;
                }
            }
        }
    } catch(permexception e) {
        std::cerr << e.what() << std::endl;
        table.close();
        THROW_PERMISSION_EXCEPTION_TRACE();
    }

    table.close();
}
