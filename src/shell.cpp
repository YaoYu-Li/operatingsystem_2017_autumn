#include <iostream>
#include <string>

#include "shell.h"
#include "fsop.h"

using namespace std;

static void print_usage (char *usage, char *func) 
{
    printf("%s.....%s%s%-40s\t%s%s%-20s\n",BOLD_END, BOLD, KBLU, usage,
                                           BOLD_END, KWHT, func);
}

static void clear_screen()
{
    printf("\033[2J\033[0;0H");
}

Shell::Shell()
{
    _prompt = "$";
}


Shell::Shell(string prompt)
{
    _prompt = prompt;
}


void Shell::show_usage()
{
    printf("\033[2J");
    printf("%s\t ____ ____ ____ ____ ____ ____ ____ \n", BOLD_END);
    printf("%s\t||%sW%s |||%se%s |||%sl%s |||%sc%s ||", BOLD_END, BOLD,
                     BOLD_END, BOLD, BOLD_END, BOLD, BOLD_END, BOLD, BOLD_END);
    printf("%s|%so%s |||%sm%s |||%se%s ||\n", BOLD_END, BOLD, BOLD_END, BOLD,
                     BOLD_END, BOLD, BOLD_END);
    printf("%s\t||__|||__|||__|||__|||__|||__|||__||\n", BOLD_END);
    printf("%s\t|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|\n\n", BOLD_END);

    char cc[] = "clean screen";
    char cf[] = "create file";
    char rm[] = "remvoe file";
    char ef[] = "edit file";
    char cp[] = "copy file";
    char mv[] = "move file";
    char ls[] = "list file";
    char tr[] = "show whole files in the filesystem";
    char fi[] = "search file";
    char ca[] = "show file content";
    char re[] = "rename file";
    char ch[] = "chmod file";
    char sm[] = "show mode";

    char cc_usg[] = "cc \x1B[0m\033[0m\x1B[34m\033[1m";
    char cf_usg[] = "cf [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char rm_usg[] = "rm [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char ef_usg[] = "ef [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char cp_usg[] = "cp [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m] [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char mv_usg[] = "mv [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m] [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char ls_usg[] = "ls [\x1B[0m\033[0mOPTION\x1B[34m\033[1m] [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char tr_usg[] = "tree [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char fi_usg[] = "find [\x1B[0m\033[0mFILE\x1B[34m\033[1m]";
    char ca_usg[] = "cat [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";
    char re_usg[] = "ren [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m] [\x1B[0m\033[0mFILE\x1B[34m\033[1m]";
    char ch_usg[] = "chm [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m] [\x1B[0m\033[0mPERM\x1B[34m\033[1m]";
    char sm_usg[] = "sm [\x1B[0m\033[0mPATH/FILE\x1B[34m\033[1m]";

    print_usage (cc_usg, cc);
    print_usage (cf_usg, cf);
    print_usage (rm_usg, rm);
    print_usage (ef_usg, ef);
    print_usage (cp_usg, cp);
    print_usage (mv_usg, mv);
    print_usage (ls_usg, ls);
    print_usage (tr_usg, tr);
    print_usage (fi_usg, fi);
    print_usage (ca_usg, ca);
    print_usage (re_usg, re);
    print_usage (ch_usg, ch);
    print_usage (sm_usg, sm);
}


void Shell::show_prompt()
{
    cout << "\033[0;33m" << _prompt << "\033[1;37m ";
}


void Shell::clear_cmd()
{
    _cmd = "";
    _vec_cmd.clear();
}


void Shell::read_input()
{
    getline(cin, _cmd);
}


void Shell::parse_input()
{
    size_t pos = 0;
    string delim = " ";
    string dupcmd = _cmd;

    while ((pos = dupcmd.find(delim)) != string::npos) {
        /* if space exists in command, parse the command */
        _vec_cmd.push_back(dupcmd.substr(0, pos));

        dupcmd.erase(0, pos + delim.length());
    }

    /* handle the command without space or the last option without space */
    _vec_cmd.push_back(dupcmd);
}

/*
 * TODO:
 * permmanager.cpp
 * permmanager.h
 */
void Shell::exec_cmd()
{
    FsOperation fs;

    if (_vec_cmd[0] == "") {
        return;
    }

    (_vec_cmd[0] == "cc") ? clear_screen() :
    (_vec_cmd[0] == "cf") ? fs.exec_create_file(_vec_cmd) :
    (_vec_cmd[0] == "rm") ? fs.exec_delete_file(_vec_cmd) :
    (_vec_cmd[0] == "ef") ? fs.exec_edit_file(_vec_cmd) :
    (_vec_cmd[0] == "cp") ? fs.exec_copy_file(_vec_cmd) :
    (_vec_cmd[0] == "mv") ? fs.exec_move_file(_vec_cmd) :
    (_vec_cmd[0] == "ls") ? fs.exec_list_files(_vec_cmd) :
    (_vec_cmd[0] == "sm") ? fs.exec_show_perm(_vec_cmd) :
    (_vec_cmd[0] == "tree") ? fs.exec_show_tree(_vec_cmd) :
    (_vec_cmd[0] == "find") ? fs.exec_search_file(_vec_cmd) :
    (_vec_cmd[0] == "cat") ? fs.exec_show_content(_vec_cmd) :
    (_vec_cmd[0] == "ren") ? fs.exec_rename_file(_vec_cmd) :
    (_vec_cmd[0] == "chm") ? fs.exec_chmod(_vec_cmd) :
                             fs.exec_show_unknown_cmd(_vec_cmd);
}
