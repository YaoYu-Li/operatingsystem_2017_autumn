#include <iostream>

#include "permexception.h"

permexception::permexception() : std::exception()
{
}

permexception::permexception(std::string msg) : std::exception(), msg(msg)
{
    err_msg = "[File : unknown][Func : unknown][Line : unknown][Msg : " + msg + "]";
}

permexception::permexception(const char* file, const char* func, const int line ,
                             std::string msg) : std::exception(), file(file), func(func), line(std::to_string(line)),
    msg(msg)
{
    err_msg = "[File : " + std::string(file) + "][Func : " + std::string(func) + "][Line : " +
              std::to_string(line) + "][Msg : " + msg + "]";
}

const char* permexception::what() const throw()
{
    return err_msg.c_str();
}
