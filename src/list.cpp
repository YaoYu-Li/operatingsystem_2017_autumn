#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

// path
#define TABLE_PATH "/tmp/fs/table.txt"
#define DISK_PATH "/tmp/fs/disk/"

// #define TABLE_PATH "/home/nemslab/tingshin/fs/table.txt"
// #define DISK_PATH "/home/nemslab/tingshin/fs/disk/"

// box-drawing characters
#define NW "┘"
#define SW "┐"
#define SE "┌"
#define NE "└"
#define NSEW "┼"
#define EW "─"
#define NSE "├"
#define NSW "┤"
#define NEW "┴"
#define SEW "┬"
#define NS "│"

// spec. of attribute
#define MAX_USER_NAME 128
#define MAX_FILE_NAME 512
#define MAX_FILE_NUM 128
#define MAX_USER_COUNT 3
#define MAX_A_LINE 1024
#define COLUMN 8

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define BOLD  "\033[1m"
#define BOLD_END  "\033[0m"



typedef struct file_infos {
    int key;
    struct tm tm_info;
    char user[MAX_USER_NAME];
    int size;
    char file_name[MAX_FILE_NAME];
    int perm[MAX_USER_COUNT];
    int hidden; //1: y, 0: n
    int type; //1: directory, 0: document
    int parent; // store parent at index ?
    int layer;
    char name[MAX_FILE_NAME];
} file_infos;


void show_usage_tree() {

    printf("Usage: tree [FILE]\n"
        "List file structure as a tree under FILEs.(FILE defaulr as '/')\n\n"
        "[EXIT STATUS]\n"
        "\t0 if OK\n"
        "\t-1 if FILE is not a directory\n"
        "\t-2 if FILE is not exist\n");
}


void show_usage_list() {

    printf("Usage: list [OPTION] [FILE]\n"
        "List information about the FILEs.(FILE defaulr as '/')\n\n"
        "[OPTION]\n"
        "\t--help\t\tdisplay usage and exit\n"
        "\t--all\t\tshow all file include hidden ones\n"
        "\t--color\t\tcolorize the output\n"
        "\t--long\t\tuse a long listing format\n"
        "\t--sort==WORD\t\tsort by WORD\n"
        "\t\t\tname\tfile name\n"
        "\t\t\tsize\tfile size\n"
        "\t\t\ttime\tcreate time\n\n"
        "[EXIT STATUS]\n"
        "\t0 if OK\n"
        "\t-1 if FILE is not a directory\n"
        "\t-2 if FILE is not exist\n"
        "\t-3 if error OPTIONS\n");
}


void free_ptr (void *ptr) {
    free(ptr);
    ptr = NULL;
}

void option_list(int c_opt, char **cmd, int *opt) {
    // an array to flag needed option
    for (int i = 0; i < 5; i++) {
        opt[i] = 0;
    }
    int cur_cmd = 0;
    int sort_used = 0;
    while (c_opt > 0) {
        if (strcmp(cmd[cur_cmd], "--help") == 0) {
            show_usage_list();
            opt[0] = 1;
            return;
        }
        else if (strcmp(cmd[cur_cmd], "--all") == 0) {
            opt[1] = 1;
        }
        else if (strcmp(cmd[cur_cmd], "--color") == 0) {
            opt[2] = 1;
        }
        else if (strcmp(cmd[cur_cmd], "--long") == 0) {
            opt[3] = 1;
        }
        else if (strncmp(cmd[cur_cmd], "--sort==", strlen("sort==")) == 0) {
            // if more than one sort option: leave
            if (sort_used == 1 ){
                fprintf(stderr, "you can only sort by one condition\n");
                opt[0] = -1;
                return;
            }
            if (strcmp(cmd[cur_cmd], "--sort==name") == 0) {
                opt[4] = 1;
                sort_used = 1;
            }
            else if (strcmp(cmd[cur_cmd], "--sort==size") == 0) {
                opt[4] = 2;
                sort_used = 1;
            }
            else if (strcmp(cmd[cur_cmd], "--sort==time") == 0) {
                opt[4] = 3;
                sort_used = 1;
            }
            else {
                // unknown sort option
                fprintf(stderr, "no such sort option\n");
                show_usage_list();
                opt[0] = -1;
                return;
            }

        }
        else {
            // unknown option
            fprintf(stderr, "no such option\n");
            show_usage_list();
            opt[0] = -1;
            return;
        }

        c_opt--;
        cur_cmd++;
    }

}

// read table.txt and store to an array
void read_table (char **table, int *line_count) {


    FILE *t_fp = fopen(TABLE_PATH, "r");
    int i = 0;
    char tmp[128] = {0};
    if (t_fp != NULL) {
        while(fgets(tmp, MAX_A_LINE, t_fp)) {
            table[i] = strdup(tmp);
            i++;
        }
    }
    *line_count = i;
    fclose(t_fp);
}

void get_time_format(char *_date, char *_time, struct tm *raw) {

    char yr[2], mth[2], dt[2], hr[2], mn[2];
    int yy, mm, dd, hh, min;

    strncpy(yr, _date, 4);
    strncpy(mth, &_date[4], 2);
    strncpy(dt, &_date[6], 2);

    strncpy(hr, _time, 2);
    strncpy(mn, &_time[2], 2);

    yy = atoi(yr);
    mm = atoi(mth);
    dd = atoi(dt);

    hh = atoi(hr);
    min = atoi(mn);

    // struct tm *create = (tm *)raw;
    // memset(*raw, 0, sizeof(struct tm));
    raw->tm_mday = dd;
    raw->tm_mon = (mm -1);
    raw->tm_year = (yy - 1900);

    raw->tm_sec = 0;
    raw->tm_min = min;
    raw->tm_hour = hh;
}


void get_file_struct(char *line, file_infos *file) {

    char **infos = (char **)calloc(sizeof(char *), COLUMN);


    // parse by ', '
    int k = 0;
    char *info = strtok(line, ", ");
    while (info) {
        infos[k] = strdup(info);
        info = strtok(NULL, ", ");
        k++;
    }

    // fill in file struct

    // key
    file->key = atoi(infos[0]);

    // create time
    get_time_format(infos[1], infos[2], &(file->tm_info));

    // user name
    strcpy(file->user, infos[3]);

    // file size
    file->size = atoi(infos[4]);

    // file name
    strcpy(file->file_name, infos[5]);

    // file permission
    for (int l = 0; l < MAX_USER_COUNT; l++) {
        file->perm[l] = infos[6][l] - '0';
    }

    // hidden flag
    file->hidden = 0;

    // file type
    if (infos[5][strlen(infos[5]) - 1] == '/')
        file->type = 1;
    else
        file->type = 0;

    file->parent = 0;
    file->layer = 0;
    // strcpy(file->name, "unknown");

    free_ptr(infos);
}


void print_info (file_infos *info) {

    char time_buf[26];
    strftime(time_buf, 26, "%Y-%m-%d %H:%M:%S", &(info->tm_info));

    printf("%d\t%s\t%s\t%d\t%s\t%d%d%d\t%d\t%d\n",
        info->key,
        time_buf,
        info->user,
        info->size,
        info->file_name,
        info->perm[0],
        info->perm[1],
        info->perm[2],
        info->hidden,
        info->type);
}

void get_file_names (file_infos *infos, char **file_names, int info_len) {

    for (int i = 0; i < info_len; i++) {
        strcpy(file_names[i], infos[i].file_name);
    }
}


void print_format (char *file_name, int layer, int *last) {

    // while (layer > 0) {
    //    printf("%s│ ", KBLU);
    //    layer--;
    // }
    int i = 0;
    while (layer > 0) {
        if (last[i] == 0 ) {
            printf("%s%s ", KBLU, NS);
        }
        else {
            printf("  ");
        }
        i++;
		layer--;
    }

    if (last[i] == 0) {
        // not the end of child
        printf("%s%s%s", KBLU, NSE, EW);
    }
    else if (last[i] == 1) {
        // the end of the list of child
        printf("%s%s%s", KBLU, NE, EW);
    }

    printf("%s%s%s%s\n", KNRM, BOLD, file_name, BOLD_END);
}

void sub_list (file_infos *infos, int file_num, int layer, int parent, int *last) {

    int order[128] = {0};
    order[0] = parent;

    int cur_mark = 1;
    for (int i = 0; i < file_num; i++) {
        if (infos[i].parent == parent) {
            order[cur_mark] = i;
            cur_mark++;
        }
    }
	// target directory
    print_format(infos[parent].name, layer, last);
    layer++;
    for (int j = 1; j < cur_mark; j++) {
        // printf("current parent: %d\n", parent);
		if (j == cur_mark - 1)
			last[layer] = 1;
		else
			last[layer] = 0;
        if (infos[order[j]].type == 1) {
            sub_list(infos, file_num, layer, order[j], last);
        }
		else {
			print_format(infos[order[j]].name, layer, last);
		}
    }
}


int tell_parent (file_infos *infos, char **file_names, int file_num) {

    char **find_slash = (char **)calloc(sizeof(char*), 128); // tmp all position of '/'
    int layer_count = -1;
    char *cmp_char = (char *)calloc(sizeof(char), 64);
    char *tmp = NULL;
    int find_p = 0;

    for (int i = 0; i < file_num; i++) {
        // find all position of '/'
        // if (strcmp(infos[i].file_name, "/") == 0) {
        //     // root!
        //     infos[i].parent = layer_count;
        // }
        for (int j = 0; j < strlen(infos[i].file_name); j++) {
            if (infos[i].file_name[j] == '/') {
                layer_count++;
                find_slash[layer_count] = &infos[i].file_name[j];
            }
        }


        if (infos[i].type == 1) {
            if (strlen(infos[i].file_name) == 1) {
                // root itself
                strcpy(infos[i].name, "/");
                find_slash[layer_count] = NULL;
                layer_count--;
            }
            else {
                strcpy(infos[i].name, find_slash[layer_count - 1] + 1);
                find_slash[layer_count] = NULL;
                layer_count--;
            }
        }
        else {
            strcpy(infos[i].name, find_slash[layer_count] + 1);
        }

        infos[i].layer = layer_count;

        if (layer_count == -1) {
            // root
            // printf("%s is root\n", infos[i].file_name);
            infos[i].parent = -2;
        }
        else {

            // copy parent name
            tmp = infos[i].file_name;
            if (infos[i].layer == 0) {
                strcat(cmp_char, "/");
            }
            else {
                while (tmp != find_slash[layer_count]) {
                    strncat(cmp_char, tmp, (size_t)1);
                    tmp++;
                }
                strncat(cmp_char, tmp, (size_t)1);
            }
            // printf("%s der parent = %s\n", infos[i].file_name, cmp_char);

            // find parent index
            for (int k = 0; k < file_num; k++) {
                if (strcmp(cmp_char, infos[k].file_name) == 0) {
                    infos[i].parent = k;
                    find_p = 1;
                    // printf("%s der parent at index %d\n", infos[i].file_name, k );
                    break;
                }
            }

            // error if not find the parent
            if (find_p == 0) {
                fprintf(stderr, "can't find parent\n");
                return -1;
            }
        }

        // if (layer_count == 0) {
        //     // under root
        //     // printf("%s der parent at index -1\n", infos[i].file_name);
        //     infos[i].parent = -1;
        // }
        // else if (layer_count == -1){
        //     // root
        //     printf("%s is root\n", infos[i].file_name);
        //     infos[i].parent = -2;
        // }
        // else {
        //     // copy parent name
        //     tmp = infos[i].file_name;
        //     while (tmp != find_slash[layer_count]) {
        //         strncat(cmp_char, tmp, (size_t)1);
        //         tmp++;
        //     }
        //     strncat(cmp_char, tmp, (size_t)1);
        //     // printf("%s der parent = %s\n", infos[i].file_name, cmp_char);
        //
        //     // find parent index
        //     for (int k = 0; k < file_num; k++) {
        //         if (strcmp(cmp_char, infos[k].file_name) == 0) {
        //             infos[i].parent = k;
        //             find_p = 1;
        //             // printf("%s der parent at index %d\n", infos[i].file_name, k );
        //             break;
        //         }
        //     }
        //
        //     // error if not find the parent
        //     if (find_p == 0) {
        //         fprintf(stderr, "can't find parent\n");
        //         return -1;
        //     }
        // }

        // printf("%d\t%s\t%s\t%d\t%d\n", i, infos[i].file_name, cmp_char, infos[i].parent, infos[i].layer);

        find_p = 0;
        tmp = NULL;
        memset(cmp_char, 0, sizeof(cmp_char));
        for (int l = 0; l < 128; l++) {
            find_slash[l] = NULL;
        }
        layer_count = -1;


    }

    free_ptr(cmp_char);
    free_ptr(find_slash);

    return 0;
}

int tree (int arg_c, char *arg_v[]) {

    char *target_path = strdup(arg_v[arg_c - 1]); // target file is specified at last argv

    if (arg_c == 1) {
        target_path = strdup("/");
    }

    // check if target path is a directory
    if (target_path[strlen(target_path) - 1] != '/') {
        fprintf(stderr, "not a directory\n");
        free_ptr(target_path);
        return -1;
    }
    char **table = (char **)calloc(sizeof(char *), MAX_FILE_NUM); // buff that stores content of table.txt

    for (int i = 0; i < MAX_FILE_NUM; i++) {
        table[i] = (char *)calloc(sizeof(char), MAX_A_LINE);
    }

    int file_num;
    read_table(table, &file_num); // get content of table.txt

    // char **lines = get_line(table, file_num);

    file_infos infos[file_num];

    // printf("key\tcreate_time\tcreate_user\tfile_size\tfile_name\tpermission\thidden\ttype\n");
    for (int j = 0; j < file_num; j++) {
        get_file_struct(table[j], &infos[j]);
        // print_info(&infos[j]);
    }

    char **file_names = (char **)malloc(sizeof(char*) * MAX_FILE_NUM);
    for (int m = 0; m < file_num; m++) {
        file_names[m] = (char *)malloc(sizeof(char) * MAX_FILE_NAME);
        memset(file_names[m], 0, sizeof(file_names));
    }

    get_file_names (infos, file_names, file_num);

    if (tell_parent(infos, file_names, file_num) != 0) {
        fprintf(stderr, "cannot tell parent\n");
    }



    // check if file is existed and store the index of target file
    int path_exist = -1;
    for (int k = 0; k < file_num; k++) {
        if (strcmp(infos[k].file_name, target_path) == 0) {
            path_exist = k;
        }
    }
    if (path_exist == -1) {
        fprintf(stderr, "no such path exists\n");
        return -2;
    }

    int last[128] = {0};
	last[0] = 1;
    // recursive print
    sub_list (infos, file_num, 0, path_exist, last);


    free_ptr(target_path);
	for (int u = 0; u < file_num; u++) {
		free_ptr(file_names[u]);
	}
    free_ptr(file_names);

    return 0;
}


void sort_by (file_infos *infos, int opt, int *order, int count) {
	
    char cmp_tmp = '0';
    int cmp_size = 0, p = 0, k = 0, swt_tmp = 0,tmp_count = count;
    time_t cmp_1;
    time_t cmp_2;
	/*
    // deal with sort
    int order_tmp[MAX_FILE_NUM] = {0};
    for (int n = 0; n < count; n++) {
        order_tmp[n] = order[n];
    }
	*/
    if (opt == 1) {
         // name
        for (p = 0; p < count; p++) {
            if (p == count - 1)
                break;
            cmp_tmp = infos[order[p]].name[0];
            for (k = p + 1; k < count; k++) {
                if (cmp_tmp > infos[order[k]].name[0]) {
                    swt_tmp = order[p];
                    order[p] = order[k];
                    order[k] = swt_tmp;
                }
            }
        }
    }
	else if (opt == 2) {
		// size
		for (p = 0; p < count; p++) {
			if (p == count -  1)
				break;
			cmp_size = infos[order[p]].size;
			for (k = p + 1; k < count; k++) {
				if (cmp_size >= infos[order[k]].size) {
					swt_tmp = order[p];
					order[p] = order[k];
					order[k] = swt_tmp;
				}
			}
		}
	}
	else if (opt == 3) {
		// time
		for (p = 0; p < count; p++) {
			if (p == count -  1)
				break;
			cmp_1 = mktime(&infos[order[p]].tm_info);
			for (k = p + 1; k < count; k++) {
				cmp_2 = mktime(&infos[order[k]].tm_info);
				if (cmp_1 >= cmp_2) {
					swt_tmp = order[p];
					order[p] = order[k];
					order[k] = swt_tmp;
				}
			}
		}
	}

}


int list (int arg_c, char *arg_v[]) {


    char *target_path = strdup(arg_v[arg_c - 1]); // target file is specified at last argv

    int opts[5] = {0};
    int opt_c = 0;
    if (arg_c > 1) {
        // find all options
        for (int y = 1; y < arg_c; y++) {
            if (strncmp(arg_v[y], "--", strlen("--")) == 0)
                opt_c++;
        }
    }

    if (arg_c - opt_c == 1) {
        target_path = strdup("/");
    }

    // check if target path is a directory
    if (target_path[strlen(target_path) - 1] != '/') {
        fprintf(stderr, "not a directory\n");
        free_ptr(target_path);
        return -1;
    }

    if (opt_c > 0) {
        option_list(opt_c, &arg_v[1], opts);
        if (opts[0] == 1) {
            // help
            return 0;
        }
        else if (opts[0] == -1) {
            return -3;
        }
    }
    char **table = (char **)calloc(sizeof(char *), MAX_FILE_NUM); // buff that stores content of table.txt

    for (int i = 0; i < MAX_FILE_NUM; i++) {
        table[i] = (char *)calloc(sizeof(char), MAX_A_LINE);
    }

    int file_num;
    read_table(table, &file_num); // get content of table.txt
    // char **lines = get_line(table, file_num);

    file_infos infos[file_num];


    // printf("key\tcreate_time\tcreate_user\tfile_size\tfile_name\tpermission\thidden\ttype\n");
    for (int j = 0; j < file_num; j++) {
        get_file_struct(table[j], &infos[j]);
        // print_info(&infos[j]);
    }

    // check if file is existed and store the index of target file
    int path_exist = -1;
    for ( int k = 0; k < file_num; k++) {
        if (strcmp(infos[k].file_name, target_path) == 0) {
            path_exist = k;
        }
    }

    if (path_exist == -1) {
        fprintf(stderr, "no such path exists\n");
        return -2;
    }


    // find all files that under the target directory
    // int *under_target = (int *)calloc(sizeof(int), file_num);
    char *slash = NULL;
    int under_count = 0; // the count of file under target path
    int under_index[MAX_FILE_NUM] = {0}; // an int array to store the index of files under target path

    // find all files that under target path
    for (int l = 0; l < file_num; l++) {
        if (l == path_exist) {
            continue;
        }
        if (strncmp(target_path, infos[l].file_name, strlen(target_path)) == 0) {
            if ((slash = strchr(infos[l].file_name + strlen(target_path), '/')) != NULL) {
                // a directory
                if (strcmp(slash + 1, "\0") == 0) {
                    // under_target[l] = 1;
                    under_index[under_count] = l;
                    under_count++;
                }
            }
            else{
                // under_target[l] = 1;
                under_index[under_count] = l;
                under_count++;
            }
        }
    }

    char color[8] = {0};
    strcpy(color, KNRM);
    char timename[128] = {0};

	
    char **file_names = (char **)malloc(sizeof(char*) * MAX_FILE_NUM);
    for (int m = 0; m < file_num; m++) {
        file_names[m] = (char *)malloc(sizeof(char) * MAX_FILE_NAME);
        memset(file_names[m], 0, sizeof(file_names));
    }

    get_file_names (infos, file_names, file_num);
    // deal with sort
    if (opts[4] > 0 && under_count > 1) {
		tell_parent (infos, file_names, file_num);
		sort_by(infos, opts[4], under_index, under_count);
    }



    for (int m = 0; m < under_count; m++) {
        if (opts[1]) {
            // all
            if (infos[under_index[m]].hidden == 1)
                continue;
        }
        if (opts[2]) {
            // color
            if (infos[under_index[m]].type == 1)
                strcpy(color, KGRN);
            else
                strcpy(color, KNRM);
        }
        printf("%s%-20s%s", color, infos[under_index[m]].file_name, KNRM);

        if (opts[3]) {
			// all
            mktime(&infos[under_index[m]].tm_info);
			memset(timename, 0, sizeof(timename));
            strncpy(timename, asctime(&infos[under_index[m]].tm_info), strlen(asctime(&infos[under_index[m]].tm_info)) - 1);
            printf("\t%s\t%d\t%s", timename, infos[under_index[m]].size, infos[under_index[m]].user);
        }
        printf("\n");
    }


/*
    for (int l = 0; l < file_num; l++) {
        // = 2 if this is the target path
        if (l == path_exist) {
            under_target[l] = 2;
            continue;
        }
        // = 1 if under the target path
        if (strncmp(target_path, infos[l].file_name, strlen(target_path)) == 0) {
            if ((slash = strchr(infos[l].file_name + strlen(target_path), '/')) != NULL) {
                // grand child
                if (strcmp(slash + 1, "\0") == 0)
                    under_target[l] = 1;
            }
            else
                under_target[l] = 1;
        }
    }

    // print output
    for (int m = 0; m < file_num; m++) {
        if (under_target[m] == 1)
            printf("%s\n", infos[m].file_name);
    }
*/
    // free_ptr(under_target);
    free_ptr(target_path);
	for (int u = 0; u < file_num; u++) {
		free_ptr(file_names[u]);
	}
    free_ptr(file_names);
    return 0;
}
/*
int main (int argc, char *argv[]) {
	if (strcmp(argv[1], "list") == 0) {
		printf("this is list\n");
        printf("argc = %d\n", argc);
		if (list(argc - 1, &argv[1]) < 0)
			fprintf(stderr, "list error\n");
	}
	else if (strcmp(argv[1], "tree") == 0) {
		printf("this is tree\n");
		if (tree(argc - 1, &argv[1]) < 0)
			fprintf(stderr, "tree error\n");
	}

    return 0;
}
*/
