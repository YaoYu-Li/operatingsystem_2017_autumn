#include<iostream>
#include<sys/stat.h>
#include<unistd.h>
#include<string>
#include<string.h>
#include<stdio.h>
#include<ctime>
#include<sstream>
#include<unistd.h>
#include<time.h>
#include<stdlib.h>
#include<vector>

#include "add_rm_edit.h"

using namespace std;

int table_index=1;
string current_user="Brook";
// current_user should be replaced by who handle the permission.

string add_previous_path(string path) {
	string result="../disk";

	if(path[0]!='/') result+='/';
	result+=path;

	return result;
}

// some small functions as tools.
string path_name(string file_path, string file_name) {
	string result="",slash="/";

	result+=file_path;
	if(file_path[file_path.size()-1]!='/') result+=slash;
	result+=file_name;

	return result;
}

bool check_existing(string file_name, string file_path) {
	FILE* file_pointer=fopen(add_previous_path(path_name(file_path, file_name)).c_str(),"r");
	if(file_pointer!=NULL) return 0;

	return 1;
}

bool check_direction(string file_path) {
	FILE* file_pointer=fopen(add_previous_path(file_path).c_str(),"r");
	if(file_pointer==NULL) return 0;

	return 1;
}

bool check_right(string file_path) {
	FILE* file_pointer=fopen("../table.txt","r");
	char buffer[7][1000];
	for(int first=0;first<7;first++)
		for(int second=0;second<1000;second++) buffer[first][second]=' ';

	while(fscanf(file_pointer,"%s%s%s%s%s%s%s",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6])>0) {
		string token[7];
		for(int transfer=0;transfer<7;transfer++) token[transfer]=buffer[transfer];
		if(token[5]==file_path+",") {
			if(token[3]==current_user+",") {
				if(token[6][0]=='7'||token[6][0]=='6'||token[6][0]=='3'||token[6][0]=='2') return 1; 
			} else {
				if(token[6][2]=='7'||token[6][2]=='6'||token[6][2]=='3'||token[6][2]=='2') return 1;
			}
		}
	}

	return 0;
}

string int_to_string(int number) {
	stringstream ss;
	ss<<number;

	return ss.str();
}

int string_to_int(string token) {
	int result=0;
	while(token.size()>0) {
		result*=10;
		result+=(int)token[0]-48;
		token.erase(0,1);
	}

	return result;
}

string get_date_time(void) {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%Y%m%d, %I%M",timeinfo);
	string result(buffer);

	return result;
}

string get_file_name(string file_path) {
	string result="";
	char current_char;
	int done=0;

	while(current_char=file_path[file_path.size()-1]) {
		if(current_char=='/'&&done!=0) break;
		result.insert(0,1,current_char);
		file_path.erase(file_path.size()-1,1);
		done=1;
	}
	
	return result;
}

string get_file_path(string file_path_name) {
	string result=file_path_name;
	while(1) {
		result.erase(result.size()-1,1);
		if(result[result.size()-1]=='/') break;
	}
	
	return result;
}

int random_number(int index) {
	srand(time(NULL));
	int result=rand()%10000;
	for(int times=0;times<10;times++) {
		result*=result;
		result%=10000;
		result+=table_index*table_index;
	}

	return result;
}

bool is_file(string path) {
	if(path[path.size()-1]=='/') return 0;
	else return 1;
}

bool include_number(vector<int> index_list, int index) {
	for(int position=0;position<index_list.size();position++) {
		if(index_list[position]==index) return 1;
	}
	return 0;
}

void delete_index(string index) {
	FILE *input_file_pointer=fopen("../table.txt","r"), *output_file_pointer=fopen("../tmp_table.txt","w");
	
	char buffer[7][1000];
	for(int first=0;first<7;first++)
		for(int second=0;second<1000;second++) buffer[first][second]=' ';

	while(fscanf(input_file_pointer,"%s%s%s%s%s%s%s",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6])>0) {
		string token(buffer[0]);
		if(index==token) continue;
		else fprintf(output_file_pointer, "%s %s %s %s %s %s %s\n",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6]);
	}

	fclose(input_file_pointer);
	fclose(output_file_pointer);
	remove("../table.txt");
	rename("../tmp_table.txt", "../table.txt");
}

string migrate_path(string left_string, int left_number, string right_string, int right_number) {
	string result="";
	result.append(left_string,0,left_number);
	result.append(right_string,right_number,right_string.size()-right_number);

	return result;
}

// some important big API.
int create_file(string file_name, string file_path, string file_owner, string file_type) {
// check a lot of things.
	if(!check_direction(file_path)) return -1;
	if(!check_existing(file_name, file_path)) return -2;

	int file_size=0;
	if(file_type=="file") {
        int pid = fork();

        if (pid == -1) {
            perror("fork()");
        } else if (pid == 0) {
            char tmp[1000] = {0};
            sprintf(tmp, "vim /tmp/fs/disk%s/%s", file_path.c_str(), file_name.c_str());
            execl("/usr/bin/gnome-terminal", "gnome-terminal", "--full-screen", "-e", tmp, NULL);
        } else {
            wait();
        }

	} else if(file_type=="direction"){
		mkdir(add_previous_path(path_name(file_path, file_name)).c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	} else {
		cout<<"Unknow file_type!\n";
		return 0;
	}
// write attributes into the table.	
	string attributes="";
	
	attributes+=int_to_string(random_number(table_index));
	attributes+=", ";
	table_index++;
	
	attributes+=get_date_time();
	attributes+=", ";

	attributes+=current_user;
	attributes+=", ";

	attributes+=int_to_string(file_size);
	attributes+=", ";

	string tmp = path_name(file_path, file_name);
    if (tmp[1] == '/')
        tmp = tmp.substr(1);
	attributes+=tmp;
	if(file_type=="direction") attributes+="/";
	attributes+=", ";

	int permission;
	if(file_type=="file") permission=664;
	else permission=775; 
	attributes+=int_to_string(permission);
	attributes.push_back('\n');		

	FILE* file_pointer=fopen("../table.txt","a");
	fprintf(file_pointer, "%s",attributes.c_str());
	fclose(file_pointer);

	return 1;
}

int delete_file(string file_path) {
//checking.
	if(!check_direction(file_path)) return -1;
	if(!check_right(file_path)) return -3;
	if(!check_direction("../disk/trash_cane")) mkdir("../disk/trash_cane",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//move files to trash cane.
    string check_name = file_path;
    while(check_direction(add_previous_path("../disk/trash_cane/" + get_file_name(check_name)))) {
        if (check_name.back() == '/') check_name.erase(check_name.size() - 1, 1);
        check_name += "(1)";
    }
    rename(add_previous_path(file_path).c_str(), add_previous_path((string)"../disk/trash_cane/" + get_file_name(check_name)).c_str());
//update the table.	
	FILE* file_pointer=fopen("../table.txt","r");
	char buffer[7][1000];
	for(int first=0;first<7;first++)
		for(int second=0;second<1000;second++) buffer[first][second]=' ';

	while(fscanf(file_pointer,"%s%s%s%s%s%s%s",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6])>0) {
		string token[7];
		size_t find;
		for(int transfer=0;transfer<7;transfer++) token[transfer]=buffer[transfer];
		find=add_previous_path(token[5]).find(add_previous_path(file_path));
		if(find!=string::npos) {
            if (file_path[file_path.size() - 1] != '/')
                if (file_path.size() != token[5].size() - 1) continue;
			fclose(file_pointer);
			delete_index(token[0]);
			file_pointer=fopen("../table.txt","r");
		}
	}

	return 1;
}

//the sub function has a little to another one.
int create_file_copy(string file_name, string file_path, string file_owner, string file_type, string original_path) {
//checking.
	if(!check_direction(file_path)) return -1;
	if(!check_existing(file_name, file_path)) return -2;
//copy file contents.
	int file_size=0;
	if(file_type=="file") {
		FILE *input_file_pointer=fopen(add_previous_path(original_path).c_str(),"r");
		FILE *output_file_pointer=fopen(add_previous_path(path_name(file_path, file_name)).c_str(),"w");
		
		char buffer[10000];
		for(int run=0;run<10000;run++) buffer[run]=' ';

		// while(fgets(input_file_pointer,"%s",buffer)>0) {
		while(fgets(buffer, sizeof(buffer), input_file_pointer)) {
			fprintf(output_file_pointer,"%s",buffer);
		}

		file_size=ftell(output_file_pointer);
		fclose(input_file_pointer);
		fclose(output_file_pointer);
	} else if(file_type=="direction"){
		mkdir(add_previous_path(path_name(file_path, file_name)).c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	} else {
		cout<<"Unknow file_type!\n";
		return 0;
	}
// update the table.	
	string attributes="";
	
	attributes+=int_to_string(random_number(table_index));
	attributes+=", ";
	table_index++;
	
	attributes+=get_date_time();
	attributes+=", ";

	attributes+=current_user;
	attributes+=", ";

	attributes+=int_to_string(file_size);
	attributes+=", ";

	attributes+=path_name(file_path, file_name);
	// if(file_type=="direction") attributes+="/";
	attributes+=", ";

	int permission;
	if(file_type=="file") permission=664;
	else permission=775; 
	attributes+=int_to_string(permission);
	attributes.push_back('\n');		

	FILE* file_pointer=fopen("../table.txt","a");
	fprintf(file_pointer, "%s",attributes.c_str());
	fclose(file_pointer);

	return 1;
}

void edit_file(char const *file_path) {
    int pid = fork();

    if (pid == -1) {
        perror("fork()");
    } else if (pid == 0) {
        char tmp[1000] = {0};
        sprintf(tmp, "vim /tmp/fs/disk%s", file_path);
        execl("/usr/bin/gnome-terminal", "gnome-terminal", "--full-screen", "-e", tmp, NULL);
    } else {
        wait();
    }
}

int copy_file(string file_path, string file_destination) {
//checking.
	if(!check_direction(file_path)) return -1;
	if(check_direction(file_destination)) return -2;
//find the target files.	
	FILE* file_pointer=fopen("../table.txt","r");
	char buffer[7][1000];
	for(int first=0;first<7;first++)
		for(int second=0;second<1000;second++) buffer[first][second]=' ';

	vector<int> index_list;
	while(fscanf(file_pointer,"%s%s%s%s%s%s%s",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6])>0) {
		string token[7],type;
		size_t find;
		for(int transfer=0;transfer<7;transfer++) token[transfer]=buffer[transfer];
		
		token[0].erase(token[0].size()-1,1);
		if(include_number(index_list,string_to_int(token[0]))) continue;

		find=add_previous_path(token[5]).find(add_previous_path(file_path));
		if(find!=string::npos) {
			index_list.push_back(string_to_int(token[0]));
			fclose(file_pointer);
			token[3].erase(token[3].size()-1,1);
			token[5].erase(token[5].size()-1,1);
			if(is_file(token[5])) type="file";
			else type="direction";

			int ignore=get_file_path(file_path).size();
			int attach=get_file_path(file_destination).size();
			string tmp_path=migrate_path(file_destination, attach, token[5], ignore);
			// string tmp_path=migrate_path(token[5], ignore, file_destination, attach);
// create new file and update table.			
			create_file_copy(get_file_name(tmp_path), get_file_path(tmp_path),token[3],type,token[5]);
			file_pointer=fopen("../table.txt","r");
		}
	}
	fclose(file_pointer);

	return 1;
}

int move_file(string file_path, string file_destination) {
// checking.
	if(!check_direction(file_path)) return -1;
	if(check_direction(file_destination)) return -2;
	if(!check_right(file_path)) return -3;
// moveing.
	rename(add_previous_path(file_path).c_str(),add_previous_path(file_destination).c_str());
// update the table.
	FILE* input_file_pointer=fopen("../table.txt","r");
	FILE* output_file_pointer=fopen("../move_file_tmp_table.txt","w");

	char buffer[7][1000];
	for(int first=0;first<7;first++)
		for(int second=0;second<1000;second++) buffer[first][second]=' ';

	while(fscanf(input_file_pointer,"%s%s%s%s%s%s%s",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6])>0) {
		string token[7];
		size_t find;

		for(int transfer=0;transfer<7;transfer++) token[transfer]=buffer[transfer];
		find=add_previous_path(token[5]).find(add_previous_path(file_path));
		if(find!=string::npos) {

			string attributes="";
	
			attributes+=int_to_string(random_number(table_index));
			attributes+=", ";
			table_index++;
	
			attributes+=get_date_time();
			attributes+=", ";

			attributes+=token[3];
			attributes+=" ";

			attributes+=token[4];
			attributes+=" ";

			token[5].erase(token[5].size()-1,1);
			int ignore=get_file_path(file_path).size();
			int attach=get_file_path(file_destination).size();
			attributes+=migrate_path(file_destination, attach, token[5], ignore);
			attributes+=", ";

			attributes+=token[6];
			attributes.push_back('\n');		
		
			fprintf(output_file_pointer, "%s",attributes.c_str());

		} else {
			fprintf(output_file_pointer,"%s %s %s %s %s %s %s\n",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6]);
		}
	}

	fclose(input_file_pointer);
	fclose(output_file_pointer);
	remove("../table.txt");
	rename("../move_file_tmp_table.txt","../table.txt");

	return 1;
}

/*
int back_from_trash_cane(string path) {
//check
	if(!check_direction(path)) return -1;
//move
	rename(add_previous_path(path).c_str(),add_previous_path(get_file_name(path)).c_str());
//update table.txt
	FILE* file_pointer=fopen(add_previous_path(get_file_name(path)).c_str(),"r");
	int file_size=0;
	file_size=ftell(file_pointer);
	fclose(file_pointer);
	string attributes="";
	
	attributes+=int_to_string(random_number(table_index));
	attributes+=", ";
	table_index++;
	
	attributes+=get_date_time();
	attributes+=", ";

	attributes+=current_user;
	attributes+=", ";

	attributes+=int_to_string(file_size);
	attributes+=", ";

	attributes+=path_name("/", get_file_name(path));
	if(!is_file(path)) attributes+="/";
	attributes+=", ";

	int permission;
	if(is_file(path)) permission=664;
	else permission=775; 
	attributes+=int_to_string(permission);
	attributes.push_back('\n');		

	file_pointer=fopen("../table.txt","a");
	fprintf(file_pointer, "%s",attributes.c_str());
	fclose(file_pointer);	
}*/


